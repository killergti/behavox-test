package com.behavox.emailtest.parser;

import com.behavox.emailtest.config.FormatModifier;
import com.behavox.emailtest.io.OutputStreamSupplier;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author ifedorenkov
 */
public class MboxParserFactoryTest {
    private static final OutputStreamSupplier SUPPLIER = () -> null;

    @Test
    public void test() {
        Assert.assertEquals(DefaultMboxParser.class, createParser().getClass());
        Assert.assertEquals(ArchivingMboxParser.class, createParser(FormatModifier.ARCHIVING).getClass());

        MboxParser parser = createParser(FormatModifier.ZIPPED);
        Assert.assertEquals(ZippedMboxParserProxy.class, createParser(FormatModifier.ZIPPED).getClass());
        Assert.assertEquals(DefaultMboxParser.class, ((ZippedMboxParserProxy) parser).unwrap().getClass());

        parser = createParser(FormatModifier.ZIPPED, FormatModifier.ARCHIVING);
        Assert.assertEquals(ZippedMboxParserProxy.class, parser.getClass());
        Assert.assertEquals(ArchivingMboxParser.class, ((ZippedMboxParserProxy) parser).unwrap().getClass());
    }

    private static MboxParser createParser(FormatModifier... formatModifiers) {
        return MboxParserFactory.createMboxParser(SUPPLIER, new HashSet<>(Arrays.asList(formatModifiers)));
    }
}

package com.behavox.emailtest.parser;

import com.behavox.emailtest.config.CustomerType;
import com.behavox.emailtest.config.FormatModifier;
import com.behavox.emailtest.io.OutputStreamSupplier;
import com.behavox.emailtest.io.UnixLineEndingFilterInputStream;
import org.apache.james.mime4j.MimeException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.*;
import java.util.stream.IntStream;

/**
 * @author ifedorenkov
 */
@RunWith(Parameterized.class)
public class ParsersTest {
    private static final Set<FormatModifier> NO_MODIFIERS = EnumSet.noneOf(FormatModifier.class);

    @Parameterized.Parameters
    public static Collection<Object[]> initTestDataSet() {
        List<Object[]> dataSet = new ArrayList<>();
        // Standard cases
        dataSet.add(new Object[] {CustomerType.CUSTOMER_A, "test-A.mbox", NO_MODIFIERS});
        dataSet.add(new Object[] {CustomerType.CUSTOMER_B, "test-B.mbox.zip", NO_MODIFIERS});
        dataSet.add(new Object[] {CustomerType.CUSTOMER_C, "test-C.mbox", NO_MODIFIERS});

        // These are cases when the default set of format modifiers is being overridden
        dataSet.add(new Object[] {CustomerType.CUSTOMER_A, "test-additional-modifiers.mbox.zip",
            EnumSet.of(FormatModifier.ZIPPED, FormatModifier.ARCHIVING)});
        dataSet.add(new Object[] {CustomerType.CUSTOMER_B, "test-additional-modifiers.mbox.zip",
            EnumSet.of(FormatModifier.ARCHIVING)});
        dataSet.add(new Object[] {CustomerType.CUSTOMER_C, "test-additional-modifiers.mbox.zip",
            EnumSet.of(FormatModifier.ZIPPED)});

        return dataSet;
    }

    private final String mboxFileName;
    private final Set<FormatModifier> formatModifiers;

    public ParsersTest(CustomerType customerType, String mboxFileName, Set<FormatModifier> additionalModifiers) {
        this.mboxFileName = mboxFileName;

        formatModifiers = new HashSet<>(customerType.getFormatModifiers());
        if (additionalModifiers != null)
            formatModifiers.addAll(additionalModifiers);
    }

    @Test
    public void testParser() throws IOException, MimeException {
        TestOutputStreamSupplier outputStreamSupplier = new TestOutputStreamSupplier();

        MboxParser parser = MboxParserFactory.createMboxParser(outputStreamSupplier, formatModifiers);

        try (InputStream input = getResourceStream("input/" + mboxFileName)) {
            parser.parse(input);
        }
        Assert.assertEquals(3, outputStreamSupplier.size());
        IntStream.range(0, 3).forEach(index -> {
            String expectedFileName = "expectedresult/expected" + (index + 1) + ".eml";
            try (InputStream expectedInputStream = new UnixLineEndingFilterInputStream(
                new BufferedInputStream(getResourceStream(expectedFileName))))
            {
                ByteBuffer result = ByteBuffer.wrap(outputStreamSupplier.get(index).toByteArray());
                ByteBuffer expected = ByteBuffer.allocate(result.capacity());
                Channels.newChannel(expectedInputStream).read(expected);
                expected.flip();
                Assert.assertEquals(expected, result);
            } catch (IOException ioe) {
                Assert.fail();
            }
        });
    }

    private static InputStream getResourceStream(String name) {
        return ParsersTest.class.getClassLoader().getResourceAsStream(name);
    }

    private static class TestOutputStreamSupplier implements OutputStreamSupplier {
        private final List<ByteArrayOutputStream> createdStreams = new ArrayList<>();

        @Override
        public OutputStream get() {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            createdStreams.add(stream);
            return stream;
        }

        /**
         * Returns the created stream at the specified index.
         *
         * @param index of the created stream
         * @return created stream
         */
        ByteArrayOutputStream get(int index) {
            return createdStreams.get(index);
        }

        /**
         * Returns the number of created streams.
         *
         * @return number of created streams
         */
        int size() {
            return createdStreams.size();
        }
    }
}

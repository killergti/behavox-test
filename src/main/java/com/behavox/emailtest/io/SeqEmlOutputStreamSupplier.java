package com.behavox.emailtest.io;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * A sequential supplier of *.eml file output streams.
 * Each invocation of the supplier produces a stream to a new file whose name contains a unique number:
 * <outputFilePrefix>1.eml, <outputFilePrefix>2.eml, etc
 *
 * @author ifedorenkov
 */
public class SeqEmlOutputStreamSupplier implements OutputStreamSupplier {
    private static final String FILE_EXTENSION = ".eml";

    private final String outputFilePrefix;
    private int counter = 0;

    public SeqEmlOutputStreamSupplier(String outputFilePrefix) {
        this.outputFilePrefix = outputFilePrefix;
    }

    @Override
    public OutputStream get() throws IOException {
        counter++;
        return new BufferedOutputStream(new FileOutputStream(outputFilePrefix + counter + FILE_EXTENSION));
    }
}

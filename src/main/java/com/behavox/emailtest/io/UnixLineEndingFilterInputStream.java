package com.behavox.emailtest.io;

import java.io.*;

/**
 * Adapts line separators to the Unix standard.
 * This is essential for Mime4j parser as it supports only the Unix line separators (LF).
 *
 * @author ifedorenkov
 */
public class UnixLineEndingFilterInputStream extends FilterInputStream {
    public UnixLineEndingFilterInputStream(BufferedInputStream in) {
        super(in);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int n = 0, c;
        do {
            c = this.read();
            if(c != -1) {
                b[off + n] = (byte) c;
                n++;
                len--;
            } else {
                return n == 0 ? -1 : n;
            }
        } while (len > 0);
        return n;
    }


    @Override
    public int read() throws IOException {
        int c = super.read();

        // There are two types of line endings that should be corrected: CRLF and CR
        if (c != '\r')
            return c;

        // Look forward one char and if it's not the LF then reset the position back
        mark(1);
        if (super.read() != '\n')
            reset();

        return '\n';
    }
}

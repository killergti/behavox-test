package com.behavox.emailtest.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A supplier of output stream(s).
 *
 * @author ifedorenkov
 */
public interface OutputStreamSupplier {
    /**
     * Returns a new output stream instance.
     *
     * @return output stream
     * @throws IOException in case of I/O error
     */
    OutputStream get() throws IOException;
}

package com.behavox.emailtest.mboxiterator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/**
 * Fix of (yet another) bug in the Mime4j lib.
 *
 * @author ifedorenkov
 */
public class CharBufferWrapper extends org.apache.james.mime4j.mboxiterator.CharBufferWrapper {
    private final CharBuffer messageBuffer;

    public CharBufferWrapper(CharBuffer messageBuffer) {
        super(messageBuffer);
        this.messageBuffer = messageBuffer;
    }

    @Override
    public InputStream asInputStream(Charset encoding) {
        return new ByteBufferInputStream(encoding.encode(messageBuffer));
    }

    /**
     * Provide an InputStream view over a ByteBuffer.
     */
    private static class ByteBufferInputStream extends InputStream {
        private final ByteBuffer buf;

        private ByteBufferInputStream(ByteBuffer buf) {
            this.buf = buf;
        }

        @Override
        public int read() throws IOException {
            return buf.hasRemaining() ? buf.get() & 0xFF : -1;
        }

        @Override
        public int read(byte[] buffer) throws IOException {
            return this.read(buffer, 0, buffer.length);
        }

        @Override
        public int read(byte[] bytes, int off, int len) throws IOException {
            if (!buf.hasRemaining())
                return -1;

            len = Math.min(len, buf.remaining());
            buf.get(bytes, off, len);
            return len;
        }
    }
}

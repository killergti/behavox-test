package com.behavox.emailtest.mboxiterator;

import org.apache.commons.io.Charsets;
import org.apache.james.mime4j.mboxiterator.FromLinePatterns;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * An iterator over mail messages that are kept within an mbox file.
 * Customized version of the {@link org.apache.james.mime4j.mboxiterator.MboxIterator} which can't be extended.
 *
 * @author ifedorenkov
 */
public class MboxIterator implements Iterable<CharBufferWrapper> {
    private final CharBuffer messageCharBuffer;
    private final ByteBuffer mboxByteBuffer;
    private final CharsetDecoder decoder;
    private final int maxMessageSize;
    private final Pattern fromLinePattern;
    private Matcher fromLineMatcher;
    private boolean fromLineFound;
    /**
     * Flag to signal end of input to {@link java.nio.charset.CharsetDecoder#decode(java.nio.ByteBuffer)} .
     */
    private boolean endOfInputFlag = false;
    private int findStart = -1;
    private int findEnd = -1;

    private MboxIterator(MboxByteBufferSource mboxBufferSource, Charset charset,
        String regexpPattern, int regexpFlags, int maxMessageSize)
        throws IOException
    {

        this.maxMessageSize = maxMessageSize;
        this.fromLinePattern = Pattern.compile(regexpPattern, regexpFlags);
        this.decoder = charset.newDecoder();
        this.messageCharBuffer = CharBuffer.allocate(maxMessageSize);
        this.mboxByteBuffer = mboxBufferSource.getBuffer();
        initMboxIterator();
    }

    private void initMboxIterator() throws IOException {
        decodeNextCharBuffer();
        fromLineMatcher = fromLinePattern.matcher(messageCharBuffer);
        fromLineFound = fromLineMatcher.find();
        if (fromLineFound) {
            saveFindPositions(fromLineMatcher);
        } else if (fromLineMatcher.hitEnd()) {
            throw new IllegalArgumentException("The given Mbox does not contain From_ lines that match the pattern '"
                + fromLinePattern.pattern() + "'! Maybe not be a valid Mbox or wrong matcher.");
        }
    }

    private void decodeNextCharBuffer() throws CharConversionException {
        CoderResult coderResult = decoder.decode(mboxByteBuffer, messageCharBuffer, endOfInputFlag);
        updateEndOfInputFlag();
        messageCharBuffer.flip();

        if (coderResult.isError()) {
            if (coderResult.isMalformed()) {
                throw new CharConversionException("Malformed input!");
            } else if (coderResult.isUnmappable()) {
                throw new CharConversionException("Unmappable character!");
            }
        }
    }

    private void updateEndOfInputFlag() {
        if (mboxByteBuffer.remaining() <= maxMessageSize)
            endOfInputFlag = true;
    }

    private void saveFindPositions(Matcher lineMatcher) {
        findStart = lineMatcher.start();
        findEnd = lineMatcher.end();
    }

    public Iterator<CharBufferWrapper> iterator() {
        return new MessageIterator();
    }

    public static class Builder {
        private MboxByteBufferSource mboxBufferSource;
        private Charset charset = Charsets.UTF_8;
        private String fromLinePattern = FromLinePatterns.DEFAULT2;
        private int flags = Pattern.MULTILINE;
        /**
         * Default max message size in chars: ~ 10MB chars. If the mbox file contains larger messages they
         * will not be decoded correctly.
         */
        private int maxMessageSize = 10 * 1024 * 1024;

        /**
         * Set the source of mbox document.
         *
         * @param mboxInputStream stream that provides an access to the mbox document
         * @param size max possible size of the file (bytes)
         * @return {@code this}
         */
        public Builder setFileInputStream(InputStream mboxInputStream, int size) {
            mboxBufferSource = new InputStreamBufferSourceMbox(mboxInputStream, size);
            return this;
        }

        public Builder setCharset(Charset charset) {
            this.charset = charset;
            return this;
        }

        public Builder setFromLinePattern(String fromLinePattern) {
            this.fromLinePattern = fromLinePattern;
            return this;
        }

        public Builder setFlags(int flags) {
            this.flags = flags;
            return this;
        }

        public Builder setMaxMessageSize(int maxMessageSize) {
            this.maxMessageSize = maxMessageSize;
            return this;
        }

        public MboxIterator build() throws IOException {
            if (mboxBufferSource == null)
                throw new IllegalStateException("ByteBufferSource must be specified.");
            return new MboxIterator(mboxBufferSource, charset, fromLinePattern, flags, maxMessageSize);
        }
    }

    private class MessageIterator implements Iterator<CharBufferWrapper> {
        public boolean hasNext() {
            return fromLineFound;
        }

        /**
         * Returns a CharBuffer instance that contains a message between position and limit.
         * The array that backs this instance is the whole block of decoded messages.
         *
         * @return CharBuffer instance
         */
        public CharBufferWrapper next() {
            final CharBuffer message;
            fromLineFound = fromLineMatcher.find();
            if (fromLineFound) {
                message = messageCharBuffer.slice();
                message.position(findEnd + 1);
                message.limit(fromLineMatcher.start() - 1);
                saveFindPositions(fromLineMatcher);
            } else {
                /* We didn't find other From_ lines this means either:
                 *  - we reached end of mbox and no more messages
                 *  - we reached end of CharBuffer and need to decode another batch.
                 */
                if (mboxByteBuffer.hasRemaining()) {
                    // decode another batch, but remember to copy the remaining chars first
                    CharBuffer oldData = messageCharBuffer.duplicate();
                    messageCharBuffer.clear();
                    oldData.position(findStart);
                    while (oldData.hasRemaining())
                        messageCharBuffer.put(oldData.get());
                    try {
                        decodeNextCharBuffer();
                    } catch (CharConversionException ex) {
                        throw new RuntimeException(ex);
                    }
                    fromLineMatcher = fromLinePattern.matcher(messageCharBuffer);
                    fromLineFound = fromLineMatcher.find();
                    if (fromLineFound) {
                        saveFindPositions(fromLineMatcher);
                    }
                    message = messageCharBuffer.slice();
                    message.position(fromLineMatcher.end() + 1);
                    fromLineFound = fromLineMatcher.find();
                    if (fromLineFound) {
                        saveFindPositions(fromLineMatcher);
                        message.limit(fromLineMatcher.start() - 1);
                    }
                } else {
                    message = messageCharBuffer.slice();
                    message.position(findEnd + 1);
                    message.limit(message.capacity() - 1);
                }
            }
            return new CharBufferWrapper(message);
        }

        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }
}

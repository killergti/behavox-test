package com.behavox.emailtest.mboxiterator;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

/**
 * Creates an mbox byte buffer from the given {@link InputStream}.
 *
 * @author ifedorenkov
 */
class InputStreamBufferSourceMbox implements MboxByteBufferSource {
    private final InputStream is;
    private final int maxMboxFileSize;

    /**
     * Ctor.
     *
     * @param is stream that provides an access to an mbox document
     * @param maxMboxFileSize the maximum possible size of the document
     */
    InputStreamBufferSourceMbox(InputStream is, int maxMboxFileSize) {
        this.is = is;
        this.maxMboxFileSize = maxMboxFileSize;
    }

    @Override
    public ByteBuffer getBuffer() throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(maxMboxFileSize);
        Channels.newChannel(is).read(byteBuffer);
        byteBuffer.flip();
        return byteBuffer;
    }
}

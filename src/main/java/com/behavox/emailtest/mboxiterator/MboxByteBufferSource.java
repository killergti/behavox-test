package com.behavox.emailtest.mboxiterator;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Source of a {@link java.nio.ByteBuffer} that represents an mbox file.
 *
 * @author ifedorenkov
 */
interface MboxByteBufferSource {
    /**
     * Returns a byte buffer.
     *
     * @return the buffer
     * @throws IOException if something goes wrong for some io reason
     */
    ByteBuffer getBuffer() throws IOException;
}

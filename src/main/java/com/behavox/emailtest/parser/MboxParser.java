package com.behavox.emailtest.parser;

import org.apache.james.mime4j.MimeException;

import java.io.IOException;
import java.io.InputStream;

/**
 * Implementations of this interface are responsible for splitting the given
 * mbox document onto individual rfc822 messages.
 *
 * @author ifedorenkov
 */
public interface MboxParser {
    /**
     * Parses the given mbox stream.
     *
     * @param mboxInputStream a stream that provides an access to an mbox document
     * @throws IOException in case of I/O error
     * @throws MimeException in case of any error during parsing
     */
    void parse(InputStream mboxInputStream) throws IOException, MimeException;
}

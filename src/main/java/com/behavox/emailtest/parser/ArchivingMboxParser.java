package com.behavox.emailtest.parser;

import com.behavox.emailtest.config.FormatModifier;
import com.behavox.emailtest.io.OutputStreamSupplier;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.message.DefaultBodyDescriptorBuilder;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.MimeConfig;
import org.apache.james.mime4j.stream.MimeTokenStream;
import org.apache.james.mime4j.stream.RecursionMode;
import org.apache.james.mime4j.util.MimeUtil;

import java.io.IOException;
import java.io.InputStream;

/**
 * An extended version of the {@link DefaultMboxParser} that is applicable when the {@link FormatModifier#ARCHIVING}
 * modifier has been specified.
 *
 * @author ifedorenkov
 */
public class ArchivingMboxParser extends DefaultMboxParser {
    private final MimeStreamParser parser;

    public ArchivingMboxParser(OutputStreamSupplier outputStreamSupplier) {
        super(outputStreamSupplier);
        MimeTokenStream tokenStream = new MimeTokenStream(MimeConfig.STRICT, new DefaultBodyDescriptorBuilder());
        tokenStream.setRecursionMode(RecursionMode.M_NO_RECURSE);
        parser = new MimeStreamParser(tokenStream);
        parser.setContentHandler(new ContentHandler());
    }

    @Override
    protected void handleMessage(InputStream messageInputStream) throws MimeException, IOException {
        parser.parse(messageInputStream);
    }

    /**
     * An internal content handler that extracts the actual mail messages from multipart attachments.
     */
    private class ContentHandler extends AbstractContentHandler {
        @Override
        public void body(BodyDescriptor bd, InputStream is) throws MimeException, IOException {
            if (MimeUtil.isMessage(bd.getMimeType()))
                ArchivingMboxParser.super.handleMessage(is);
        }
    }
}

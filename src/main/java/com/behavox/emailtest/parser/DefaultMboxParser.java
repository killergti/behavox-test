package com.behavox.emailtest.parser;

import com.behavox.emailtest.config.ApplicationProperties;
import com.behavox.emailtest.io.OutputStreamSupplier;
import com.behavox.emailtest.io.UnixLineEndingFilterInputStream;
import com.behavox.emailtest.mboxiterator.MboxIterator;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.mboxiterator.CharBufferWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * Default implementation of the parser.
 *
 * @author ifedorenkov
 */
public class DefaultMboxParser implements MboxParser {
    private static final CharsetEncoder ENCODER = Charset.forName(
        ApplicationProperties.getProperty("encoding")).newEncoder();
    private static final int MBOX_FILE_MAX_SIZE = ApplicationProperties.getIntProperty("mbox_file_max_size");
    private static final int MAX_MESSAGE_SIZE = ApplicationProperties.getIntProperty("message_max_size");

    private static final Logger log = LogManager.getLogger(DefaultMboxParser.class);

    private final OutputStreamSupplier outputStreamSupplier;

    /**
     * @param outputStreamSupplier supplier of streams to be used for persisting the parsed messages
     */
    public DefaultMboxParser(OutputStreamSupplier outputStreamSupplier) {
        this.outputStreamSupplier = outputStreamSupplier;
    }

    @Override
    public void parse(InputStream mboxInputStream) throws MimeException, IOException {
        MboxIterator mboxIterator = new MboxIterator.Builder()
            .setFileInputStream(prepareInputStream(mboxInputStream), MBOX_FILE_MAX_SIZE)
            .setMaxMessageSize(MAX_MESSAGE_SIZE)
            .setCharset(ENCODER.charset())
            .build();

        int count = 0;
        for (CharBufferWrapper message : mboxIterator) {
            count++;
            handleMessage(message.asInputStream(ENCODER.charset()));
        }
        log.debug("Successfully parsed {} messages", count);
    }

    /**
     * Handles the found message.
     *
     * @param messageInputStream stream that provides an access to the message
     * @throws MimeException if the message can not be processed
     * @throws IOException in case of I/O error
     */
    protected void handleMessage(InputStream messageInputStream) throws MimeException, IOException {
        try (OutputStream out = outputStreamSupplier.get()) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = messageInputStream.read(buffer)) != -1)
                out.write(buffer, 0, bytesRead);
        }
    }

    /**
     * Configures the input stream (e.g. wraps by some filter).
     *
     * @param is to be configured
     * @return configured input stream
     */
    private static InputStream prepareInputStream(InputStream is) {
        return new UnixLineEndingFilterInputStream(new BufferedInputStream(is));
    }
}

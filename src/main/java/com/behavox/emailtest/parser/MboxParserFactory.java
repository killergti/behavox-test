package com.behavox.emailtest.parser;

import com.behavox.emailtest.config.FormatModifier;
import com.behavox.emailtest.io.OutputStreamSupplier;

import java.util.Set;

/**
 * A factory of {@link MboxParser} that knows how and what exactly to create.
 *
 * @author ifedorenkov
 */
public class MboxParserFactory {
    private MboxParserFactory() {
    }

    /**
     * Creates an mbox parser instance.
     *
     * @param outputStreamSupplier supplier of streams to be used for persisting the parsed messages
     * @param modifiers a set of format modifiers
     * @return configured mbox parser instance
     */
    public static MboxParser createMboxParser(OutputStreamSupplier outputStreamSupplier,
        Set<FormatModifier> modifiers)
    {
        if (modifiers.isEmpty())
            return new DefaultMboxParser(outputStreamSupplier);

        MboxParser parser = modifiers.contains(FormatModifier.ARCHIVING) ?
            new ArchivingMboxParser(outputStreamSupplier) : new DefaultMboxParser(outputStreamSupplier);

        if (modifiers.contains(FormatModifier.ZIPPED))
            parser = new ZippedMboxParserProxy(parser);

        return parser;
    }
}

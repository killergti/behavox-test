package com.behavox.emailtest.parser;

import org.apache.james.mime4j.MimeException;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * A proxy over the {@link MboxParser} that adds an ability to work with zipped mbox documents.
 *
 * @author ifedorenkov
 */
public class ZippedMboxParserProxy implements MboxParser {
    private final MboxParser parser;

    public ZippedMboxParserProxy(MboxParser parser) {
        this.parser = parser;
    }

    @Override
    public void parse(InputStream mboxInputStream) throws IOException, MimeException {
        ZipInputStream zipStream = new ZipInputStream(mboxInputStream);
        ZipEntry zipEntry = zipStream.getNextEntry();

        if (zipEntry == null)
            throw new IOException("Failed to find the expected mbox file whithin the archive.");

        parser.parse(zipStream);
    }

    /**
     * Returns the original proxied parser.
     *
     * @return the original parser
     */
    public MboxParser unwrap() {
        return parser;
    }
}

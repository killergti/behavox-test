package com.behavox.emailtest.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.StringJoiner;

/**
 * @author ifedorenkov
 */
public class ApplicationProperties {
    private static final String PROPERTIES_FILE = "application.properties";
    private static final String DEFAULT_PROPERTY_PREFIX = "com.behavox.emailtest";

    private static Properties applicationProperties;

    static {
        applicationProperties = new Properties();
        try (InputStream in = ApplicationProperties.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE)) {
            applicationProperties.load(in);
        } catch (IOException ioe) {
            throw new ExceptionInInitializerError(ioe);
        }
    }

    /**
     * Returns a property value by constructing the property key from the default prefix and
     * the given keys.
     *
     * @param propertyKeys to be used for constructing the property key
     * @return property value
     */
    public static String getProperty(String... propertyKeys) {
        StringJoiner sj = new StringJoiner(".");
        sj.add(DEFAULT_PROPERTY_PREFIX);
        Arrays.stream(propertyKeys).forEach(sj::add);
        return applicationProperties.getProperty(sj.toString());
    }

    /**
     * Returns an integer property value by constructing the property key from the default prefix and
     * the given keys.
     *
     * @param propertyKeys to be used for constructing the property key
     * @return property value or {@code 0} if the property could not be found or can't be parsed
     */
    public static int getIntProperty(String... propertyKeys) {
        String value = getProperty(propertyKeys);
        if (value != null && !value.isEmpty()) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException ignore) {
            }
        }
        return 0;
    }
}

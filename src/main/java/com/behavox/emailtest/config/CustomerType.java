package com.behavox.emailtest.config;

import java.util.*;

/**
 * Defines customer types.
 *
 * @author ifedorenkov
 */
public enum CustomerType {
    CUSTOMER_A("CUSTOMER_A"),
    CUSTOMER_B("CUSTOMER_B", FormatModifier.ZIPPED),
    CUSTOMER_C("CUSTOMER_C", FormatModifier.ARCHIVING);

    private final String name;
    private final Set<FormatModifier> formatModifiers;

    CustomerType(String name, FormatModifier... formatModifiers) {
        this.name = name;
        this.formatModifiers = new HashSet<>(Arrays.asList(formatModifiers));
    }

    public String getName() {
        return name;
    }

    /**
     * Returns the default set of format modifiers that are used by the customer.
     *
     * @return set of format modifiers
     */
    public Set<FormatModifier> getFormatModifiers() {
        return formatModifiers;
    }

    /**
     * Returns type by its name.
     *
     * @return customer type
     */
    public static CustomerType getByName(String name) {
        for (CustomerType customerType : values()) {
            if (customerType.name.equals(name))
                return customerType;
        }
        throw new IllegalArgumentException("Unknown customer type: " + name);
    }
}

package com.behavox.emailtest.config;

/**
 * Defines all the possible modifications to the canonical process/method of storing emails within an MBOX file.
 * Canonical means a standard MBOX file that contains a number of emails we are interested in.
 *
 * @author ifedorenkov
 */
public enum FormatModifier {
    /**
     * Implies that an MBOX file is kept within a ZIP archive.
     */
    ZIPPED("ZIPPED"),

    /**
     * All the emails (we are interested in) are kept ('archived') as a multipart attachments within an mbox file.
     */
    ARCHIVING("ARCHIVING");

    private final String name;

    FormatModifier(String name) {
        this.name = name;
    }

    /**
     * Returns format modifier by its name.
     *
     * @return modifier
     */
    public static FormatModifier getByName(String name) {
        for (FormatModifier modifier : values()) {
            if (modifier.name.equals(name))
                return modifier;
        }
        throw new IllegalArgumentException("Unknown format modifier: " + name);
    }
}

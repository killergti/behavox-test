import com.behavox.emailtest.config.CustomerType;
import com.behavox.emailtest.config.FormatModifier;
import com.behavox.emailtest.io.SeqEmlOutputStreamSupplier;
import com.behavox.emailtest.parser.MboxParser;
import com.behavox.emailtest.parser.MboxParserFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author ifedorenkov
 */
public class Main {
    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        if (args.length < 2)
            printUsageAndExit("Wrong number of arguments.");

        // Extract the customer type
        CustomerType customerType = CustomerType.getByName(args[0]);

        // Extract input file path and verify that it is readable
        Path filePath = Paths.get(args[1]);
        if (!Files.exists(filePath) || !Files.isReadable(filePath))
            printUsageAndExit("File doesn't exist or can't be opened for reading.");

        // Obtain a set of format modifiers by customer type and add additional ones (if specified)
        Set<FormatModifier> formatModifiers = new HashSet<>(customerType.getFormatModifiers());
        if (args.length == 3) {
            Set<FormatModifier> additionalModifiers = Arrays.stream(args[2].split(","))
                .map(FormatModifier::getByName)
                .collect(Collectors.toSet());
            formatModifiers.addAll(additionalModifiers);
        }

        // Create an output directory
        log.debug("Initializing");
        Path outputDirPath = filePath.resolveSibling("output");
        if (Files.exists(outputDirPath)) {
            Files.walkFileTree(outputDirPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException ioe) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        }
        Files.createDirectory(outputDirPath);

        log.debug("Begin parsing");
        long start = System.currentTimeMillis();
        try (InputStream is = Files.newInputStream(filePath)) {
            MboxParser parser = MboxParserFactory.createMboxParser(
                new SeqEmlOutputStreamSupplier(outputDirPath + "/parsed"), formatModifiers);
            parser.parse(is);
        }
        long end = System.currentTimeMillis();
        log.debug("Done in {} millis", end - start);
    }

    private static void printUsageAndExit(String message) {
        System.err.println(message);
        System.err.println("Usage: Main <customer_type> <mbox_file> [<mbox_file_format>]");
        System.exit(-1);
    }
}
